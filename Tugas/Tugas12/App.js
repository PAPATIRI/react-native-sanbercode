import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import VideoItem from "./components/videoItem";
import data from "./data.json";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* navbar section */}
        <View style={styles.navBar}>
          <Image
            source={require("./images/logo.png")}
            style={{ width: 98, height: 22 }}
          />
          <View style={styles.rightNav}>
            <TouchableOpacity>
              <MaterialIcons
                style={styles.navItem}
                name="search"
                size={24}
                color="black"
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Ionicons
                style={styles.navItem}
                name="md-contact"
                size={24}
                color="black"
              />
            </TouchableOpacity>
          </View>
        </View>
        {/* body secton */}
        <View style={styles.body}>
          <FlatList
            data={data.items}
            renderItem={(video) => <VideoItem video={video.item} />}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={() => (
              <View style={{ height: 1, backgroundColor: "#999999" }} />
            )}
          />
        </View>
        {/* tab bar bottom section */}
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="home" size={24} color="black" />
            <Text>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="whatshot" size={24} color="black" />
            <Text>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="subscriptions" size={24} color="black" />
            <Text>Subscriptions</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="folder" size={24} color="black" />
            <Text>Library</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    marginTop: 12,
    width: width,
    backgroundColor: "white",
    elevation: 2,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  rightNav: {
    flexDirection: "row",
  },
  navItem: {
    marginLeft: 25,
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: "white",
    height: 60,
    borderTopWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  tabTitle: {
    fontSize: 11,
    color: "#3c3c3c",
    paddingTop: 5,
  },
});
