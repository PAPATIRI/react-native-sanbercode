import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";

export default class Kontak extends Component {
  render() {
    return (
      <View style={styles.containerPortofolio}>
        <Text style={styles.subtitle}>Hubungi Saya</Text>
        <View style={styles.listKontak}>
          <TouchableOpacity style={styles.listItemKontak}>
            <AntDesign name="facebook-square" size={50} color="#3b5998" />
            <Text style={styles.name}>@RoseareRosie</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.listItemKontak}>
            <AntDesign name="instagram" size={50} color="#c32aa3" />
            <Text style={styles.name}>@RoseBlackPink</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.listItemKontak}>
            <AntDesign name="twitter" size={50} color="#00acee" />
            <Text style={styles.name}>@chareyoung</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerPortofolio: {
    flex: 1,
    backgroundColor: "#EFEFEf",
    padding: 10,
  },
  subtitle: {
    flex: 1,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: "black",
    borderBottomColor: "#003366",
    color: "#003366",
  },
  listKontak: {
    flex: 7,
    justifyContent: "space-around",
  },
  listItemKontak: {
    paddingLeft: 50,
    paddingRight: 50,
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row",
  },
  name: {
    fontSize: 12,
    color: "#003366",
  },
});
