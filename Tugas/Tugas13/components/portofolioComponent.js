import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";

export default class Portofolio extends Component {
  render() {
    return (
      <View style={styles.containerPortofolio}>
        <Text style={styles.subtitle}>Portofolio</Text>
        <View style={styles.listPortofolio}>
          <TouchableOpacity style={styles.listItemPortofolio}>
            <AntDesign name="github" size={50} color="#171515" />
            <Text style={styles.name}>@RosePink</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.listItemPortofolio}>
            <AntDesign name="gitlab" size={50} color="#fca326" />
            <Text style={styles.name}>@RoseBlack</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerPortofolio: {
    flex: 1,
    backgroundColor: "#EFEFEf",
    padding: 10,
  },
  subtitle: {
    flex: 1,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: "#003366",
    color: "#003366",
  },
  listPortofolio: {
    flex: 2,
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10,
  },
  listItemPortofolio: {
    alignItems: "center",
    justifyContent: "center",
  },
  name: {
    fontSize: 12,
    color: "#003366",
  },
});
