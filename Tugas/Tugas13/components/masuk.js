import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import Icon from "./namaIkon";

export default class Daftar extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.icon}>
          <Icon />
        </View>
        <Text style={styles.titlePage}>Login</Text>
        <View style={styles.form}>
          <Text style={{ fontSize: 16, paddingBottom: 5, color: "#003366" }}>
            Username / Email
          </Text>
          <TextInput
            style={styles.inputText}
            autoComplete="name"
            placeholder={"nama atau email"}
          />
        </View>
        <View style={styles.form}>
          <Text style={{ fontSize: 16, paddingBottom: 5, color: "#003366" }}>
            Password
          </Text>
          <TextInput style={styles.inputText} placeholder={"password anda"} />
        </View>
        <TouchableOpacity style={styles.buttonMasuk}>
          <Text style={styles.fontButton}>Masuk</Text>
        </TouchableOpacity>
        <Text style={{ color: "#3EC6FF", fontSize: 14, paddingTop: 10 }}>
          atau
        </Text>
        <TouchableOpacity style={styles.buttonDaftar}>
          <Text style={styles.fontButton}>Daftar</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  icon: {
    alignItems: "center",
  },
  titlePage: {
    fontSize: 20,
    paddingTop: 20,
    paddingBottom: 20,
    color: "#003366",
  },
  form: {
    width: width,
    paddingRight: 40,
    paddingLeft: 40,
    paddingTop: 15,
  },
  inputText: {
    padding: 10,
    height: 40,
    borderColor: "#003366",
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "darkblue",
  },
  buttonDaftar: {
    marginTop: 10,
    backgroundColor: "#003366",
    paddingRight: 25,
    paddingLeft: 25,
    paddingTop: 7,
    paddingBottom: 7,
    borderRadius: 10,
  },
  buttonMasuk: {
    marginTop: 20,
    backgroundColor: "#3EC6FF",
    paddingRight: 25,
    paddingLeft: 25,
    paddingTop: 7,
    paddingBottom: 7,
    borderRadius: 10,
  },
  fontButton: {
    color: "white",
    fontSize: 16,
  },
});
