import React, { Component } from "react";
import { StyleSheet, View, Text, Dimensions, Image } from "react-native";

export default class Tentang extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.title}>tentang saya</Text>
        <Image
          source={{
            uri:
              "https://www.wowkeren.com/display/images/photo/2019/08/30/00270805.jpg",
          }}
          style={styles.image}
        />
        <Text style={styles.name}>Rose BlackPink</Text>
        <Text style={styles.job}>React Native Developer</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    alignItems: "center",
    padding: 10,
  },
  title: {
    fontSize: 22,
    color: "#003366",
    fontWeight: "bold",
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 100,
    marginTop: 15,
  },
  name: {
    color: "#003366",
    marginTop: 10,
    fontSize: 14,
  },
  job: {
    marginTop: 5,
    fontSize: 12,
    color: "#3EC6FF",
  },
});
