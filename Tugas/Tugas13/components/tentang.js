import React, { Component } from "react";
import { StyleSheet, View, Text, Dimensions } from "react-native";
import Header from "./fotoTentangComponent";
import Portofolio from "./portofolioComponent";
import Kontak from "./kontakComponent";

export default class Tentang extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header />
        </View>
        <View style={styles.portofolio}>
          <Portofolio />
        </View>
        <View style={styles.kontak}>
          <Kontak />
        </View>
      </View>
    );
  }
}
const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    paddingRight: 20,
    paddingLeft: 20,
  },
  header: {
    flex: 2,
    marginBottom: 30,
  },
  portofolio: {
    flex: 1,
    marginBottom: 10,
  },
  kontak: {
    flex: 2,
  },
});
