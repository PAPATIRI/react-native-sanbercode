import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import DaftarPage from "./components/daftar";
import MasukPage from "./components/masuk";
import TentangSaya from "./components/tentang";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.form}>
          {/* <DaftarPage /> */}
          {/* <MasukPage /> */}
          <TentangSaya />
        </View>
      </View>
    );
  }
}
const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  form: {
    padding: 25,
  },
});
