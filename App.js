import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
// import YoutubeUi from "./Tugas/Tugas12/App";
import Tugas13 from "./Tugas/Tugas13/App";
import Tugas14 from "./Tugas/Tugas14/App";

export default function App() {
  return (
    <View style={styles.container}>
      {/* <YoutubeUi /> */}
      {/* <Tugas13 /> */}
      <Tugas14 />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
